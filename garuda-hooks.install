# Run garuda hooks related commands.

Lsb_release() {
  local file=/etc/lsb-release

  if [ -z "$(grep "^DISTRIB_CODENAME=" $file)" ] ; then
        # add missing DISTRIB_CODENAME=
        echo "DISTRIB_CODENAME=rolling" >> $file
  fi
  sed -i /etc/lsb-release \
        -e 's,DISTRIB_ID=.*,DISTRIB_ID=Garuda,' \
        -e 's,DISTRIB_RELEASE=.*,DISTRIB_RELEASE=Soaring,' \
        -e 's,DISTRIB_CODENAME=.*,DISTRIB_CODENAME=Spotted-Eagle,' \
        -e 's,DISTRIB_DESCRIPTION=.*,DISTRIB_DESCRIPTION=\"Garuda Linux\",'
}

Os_release() {
  local file=/usr/lib/os-release
    
  sed -i /usr/lib/os-release \
        -e 's,NAME=.*,NAME=\"Garuda Linux\",' \
        -e 's,PRETTY_NAME=.*,PRETTY_NAME=\"Garuda Linux\",' \
        -e 's,ID=.*,ID=garuda,' \
        -e 's,ID_LIKE=.*,ID_LIKE=arch,' \
        -e 's,BUILD_ID=.*,BUILD_ID=rolling,' \
        -e 's,HOME_URL=.*,HOME_URL=\"https://garudalinux.org/\",' \
        -e 's,DOCUMENTATION_URL=.*,DOCUMENTATION_URL=\"https://wiki.garudalinux.org/\",' \
        -e 's,SUPPORT_URL=.*,SUPPORT_URL=\"https://forum.garudalinux.org/\",' \
        -e 's,BUG_REPORT_URL=.*,BUG_REPORT_URL=\"https://gitlab.com/groups/garuda-linux/\",' \
        -e 's,LOGO=.*,LOGO=garudalinux,'
        
  if [ -z "$(grep "^ID_LIKE=" $file)" ] && [ -n "$(grep "^ID=" $file)" ] ; then
      # add missing ID_LIKE=
      sed -i $file -e '/^ID=/a \ID_LIKE=arch'
  fi
}

Issues() {
  sed -i 's|Arch|Garuda|g' /etc/issue /usr/share/factory/etc/issue
}

Fix_nsswitch() {
  sed -i 's|systemd||g' /etc/nsswitch.conf && sed -i 's|hosts:.*|hosts: files mymachines myhostname mdns_minimal [NOTFOUND=return] resolve [!UNAVAIL=return] dns wins|g' /etc/nsswitch.conf
}

Fix_pacman() {
  sed -i 's|Server = http://lonewolf-builder.duckdns.org/$repo/x86_64|Include = /etc/pacman.d/chaotic-mirrorlist|g' /etc/pacman.conf
  sed -i 's|Server = http://chaotic.bangl.de/$repo/x86_64||g' /etc/pacman.conf
  sed -i 's|Server = https://repo.kitsuna.net/x86_64||g' /etc/pacman.conf
  sed -i 's|Server = http://lonewolf-ipfs.pedrohlc.com/$repo/x86_64||g' /etc/pacman.conf
  sed -i 's|Server = https://chaotic.tn.dedyn.io/x86_64||g' /etc/pacman.conf
  sed -i 's|.*TotalDownload|ParallelDownloads = 5|g' /etc/pacman.conf
}

Grub-btrfs() {
  sed -i /etc/default/grub-btrfs/config \
        -e 's,.*GRUB_BTRFS_SUBMENUNAME=.*,GRUB_BTRFS_SUBMENUNAME=\"Garuda Linux snapshots\",'
  sed -i /usr/lib/systemd/system/grub-btrfs.path \
        -e 's,.*PathModified=.*,PathModified=/run/timeshift/backup/timeshift-btrfs/snapshots,'
}

Grub-splash() {
  # Use an invalid cmdline parameter here so we can revert this change in the future if SDDM gets fixed
  sed -i 's|quiet garuda_splash_hotfix|quiet splash|g' /etc/default/grub
}

os-prober_fix() {
  /usr/bin/grub-fix-initrd-generation
}

nohang_fix() {
  sed -i 's|zram_checking_enabled = False|zram_checking_enabled = True|g' /etc/nohang/nohang-desktop.conf
}

timeshift_fix() {
  sed -i 's|"btrfs_mode" : "false",|"btrfs_mode" : "true",|g' /etc/timeshift/default.json
}


limits_fix() {
  sed -i /etc/systemd/system.conf \
        -e 's,.*DefaultLimitNOFILE=.*,DefaultLimitNOFILE=1048576,' \
        -e 's,.*DefaultTimeoutStopSec=.*,DefaultTimeoutStopSec=30s,'
        
  sed -i /etc/systemd/user.conf \
        -e 's,.*DefaultLimitNOFILE=.*,DefaultLimitNOFILE=1048576,' \
        -e 's,.*DefaultTimeoutStopSec=.*,DefaultTimeoutStopSec=30s,'
}

btrfs_commit_fix() {
  sed -i 's|,commit=120||g' /etc/fstab
}

Fix_openswap() {
sed -i /etc/openswap.conf \
        -e 's,.*keyfile_device_mount_options=.*,keyfile_device_mount_options="--options=subvol=@",'
}

post_install() {
  post_upgrade
  systemctl enable linux-modules-cleanup
  /usr/bin/enable-os-prober
}

post_upgrade() {
  Lsb_release
  Os_release
  Issues
  Fix_nsswitch
  Fix_pacman
  Grub-btrfs
  Grub-splash
  os-prober_fix
  nohang_fix
  timeshift_fix
  limits_fix
  btrfs_commit_fix
  Fix_openswap
  systemctl is-active linux-modules-cleanup >/dev/null || systemctl enable linux-modules-cleanup
}
